import json
import pickle
from flask import Flask, request, jsonify
import pandas as pd

def load_pickle_model(file_path_sav):
    loaded_model = pickle.load(open(file_path_sav, 'rb'))
    return loaded_model


# Load the saved Scikit-learn model
model = load_pickle_model('final_model.sav')

# Create a Flask app
app = Flask(__name__)

@app.route("/")
def root():
    return "API OK!"

# Define the API endpoint
@app.route('/predict', methods=['POST'])
def predict():
    # Get the input JSON data
    input_data = request.get_json()
    df = pd.read_json(json.dumps(input_data))

    # Make a prediction using the loaded model
    prediction = model.predict(df)
    json_response = pd.DataFrame(prediction).to_json(orient='records')
    # Return the prediction as a JSON response
    return jsonify(json_response)

# Start the Flask app using Gunicorn and Nginx
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)