FROM python:3.9-slim-buster

WORKDIR /app

COPY houston_to_dockerize /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 3000

CMD ["gunicorn", "--bind", "0.0.0.0:3000", "creation_api:app"]